const operations = ['6**2', '**', '3**3', '4**', '4**5', '8**2**', '4*=5'];

resolveOperations(operations);

function numericNotEmpty(number) {
    return number === '' || isNaN(number)
}

function numberInicioFinal(operation) {
  const numbers = operation.split("**");

  if (numbers.length === 2 && !numbers.some(numericNotEmpty)) {
    return true;
  }

  return false;
}

function resolveOperations(operations) {
    try {
      operations.map((operation) => {
        if (operation.includes("**") && numberInicioFinal(operation)) {
          console.log("Resultado = " + eval(operation));
        } else {
          console.log(null);
        }
      });
    } catch (err) {
      console.log(err);
    }
  }